use chrono::datetime;

#[derive(Debug)]
pub struct Orgdoc{
    headline: Box<Option<Headline>>,
    keywords: Vec<Keyword>,
    body: Box<Content>,
    subdocs: Vec<Box<Orgdoc>>,
}

#[derive(Debug)]
pub struct Keyword{
    name: String,
    value: String,
}

#[derive(Debug)]
pub struct Headline{
    stars: u8,
    keyword: Option<Todoflag>,
    priority: Option<Priority>,
    title: Vec<InlineContent>,
    tags: Vec<Tag>,
}

#[derive(Debug)]
pub enum Todoflag {
    Todo,
    Done,
    Queued,
    Canceled,
}

#[derive(Debug)]
pub enum Object {
    LatexFragment(String),
    ExportSnippet(String),
    FootnoteRef(String),
    BabelCall(String),
    LineBreak,
    RadioLink(String),
    Link(String,String),
    Target(String),
    Macro(String,Vec<String>),
    Superscript(Box<Object>),
    Subscript(Vec<Box<Object>>),
    Markedup(Markup,Vec<Box<Object>>),
    Paragraph(Vec<Box<Objec>>),
    Timestamp(datetime::DateTime),
}

#[derive(Debug)]
pub enum Markup{
    Bold,
    Italic,
    Underline,
    Strikethrough,
    Code,
}

#[derive(Debut)]
pub struct Priority(char);

#[derive(Debug)]
pub struct Content(Vec<Object>);

#[derive(Debug)]
pub struct ContentList(Vec<Content>);

#[derive(Debug)]
pub struct Table {
    rows: Vec<Vec<String>>,
    expressions: Vec<String>,
}
