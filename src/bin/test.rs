extern crate regex;

enum Test<'a> {
    One(&'a str),
    Two,
}

fn test<'a>(ts: &'a mut Test) {
    *ts = Test::Two;
    test2(ts);
}

fn test2<'a>(ts: &'a mut Test) {
    *ts = Test::One("bar");
}

fn main(){
    let foo = String::from("asdf");
    let mut tst = Test::One(foo.as_str());
    test(&mut tst);
}

