use org::{self,Scanner,Object,ObjectIter,Markup};
use std::rc::Rc;
use std::cell::RefCell;
use regex::{RegexSet};

#[derive(Debug,PartialEq)]
pub struct Drawer {
    pub name: String,
    pub content: Vec<Object>,
}

impl Drawer {
    pub fn parse(scann: Rc<RefCell<Scanner>>) -> Result<Drawer,org::Error> {
        let name_rx = regex!(r"^:([A-Za-z0-9_\s]+):\s*$");
        let disallow = RegexSet::new(&[
            r"^\*",                  // headline
            ]);
        let mut scanner = scann.borrow_mut();
        if let Some(name) = scanner.get_regex_line(&name_rx).and_then(|c| c.at(1)) {
            println!("got name: {:?}", name);
            let mut content = String::new();
            while let Some(ref l) = scanner.get_line() {
                if l.trim().to_uppercase() == ":END:" {
                    let inner = Scanner::makerc(content.as_str());
                    let objects:Result<Vec<Object>,org::Error> =
                        ObjectIter::new(1000,inner.clone()).collect();
                    if inner.borrow_mut().get_char().is_some() {
                        return scanner.make_err("Error: not all input in the drawer parsed!");
                    }
                    return objects.map(|obj|
                                       Drawer{
                                           name: name.to_string(),
                                           content: obj,
                                       });
                }
                content.extend(l.chars());
            }
            return scanner.make_err("Drawer input ended before :END: tag");
        } else {
            scanner.make_err("Invalid state in parse drawer.")
        }
    }
}


#[test]
fn empty(){
    let scanner = Scanner::makerc(":TEST_DRAWER:\n:END:\n");
    let rf = Drawer{
        name: "TEST_DRAWER".to_string(),
        content: vec!(),
    };
    let result = Drawer::parse(scanner);
    println!("Result: {:?}", result);
    assert!(result.is_ok());
    assert!(result.unwrap() == rf);
}

#[test]
fn content(){
    let scanner = Scanner::makerc(r":DRAWER:
This is the content
and the second line of the content.

:END:");
    let rf = Drawer{
        name: "DRAWER".to_string(),
        content: vec![
            Object::Paragraph(vec![
                Markup::Text("This is the content\nand the second line of the content.\n".to_string())])],
    };
    let result = Drawer::parse(scanner);
    println!("Result: {:?}", result);
    assert!(result.is_ok());
    assert!(result.unwrap() == rf);
}

#[test]
fn reject(){
     let scanner = Scanner::makerc(r":DRAWER: a
This is the content
and the second line of the content.

:END:");
    let result = Drawer::parse(scanner);
    println!("Result: {:?}", result);
    assert!(result.is_err());
}

#[test]
fn reject2(){
     let scanner = Scanner::makerc(r":DRAWER:
This is the content
and the second line of the content.

");
    let result = Drawer::parse(scanner);
    println!("Result: {:?}", result);
    assert!(result.is_err());
}

#[test]
fn reject3(){
    let scanner = Scanner::makerc(r":DRAWER:
* no headlines allowed!

:END:");
    let result = Drawer::parse(scanner);
    println!("Result: {:?}", result);
    assert!(result.is_err());
}
