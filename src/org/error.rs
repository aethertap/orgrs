use std::{error,io,num,fmt};
use std::convert::From;

pub struct Error {
    pub desc: String,
    pub src: Option<Box<error::Error>>,
    pub pos: (usize /*line*/,usize /*column*/),
}

pub fn make_err<T>(s:&str) -> Result<T,Error>{
    Err(Error::from(s))
}

impl error::Error for Error {
    fn description(&self) -> &str {
        &self.desc[..]
    }
    fn cause(&self) -> Option<&error::Error> {
        if let Some(ref e) = self.src {
            Some(e.as_ref())
        } else {
            None
        }
    }
}

impl fmt::Display for Error {
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "Parse Error: {}\n", self.desc);
        if let Some(ref e) = self.src {
            write!(f, "\t{}\n", e)
        } else {
            Ok(())
        }
    }
}

impl fmt::Debug for Error{
    fn fmt(&self, f:&mut fmt::Formatter) -> fmt::Result {
        write!(f, "Error {{desc:\"{}\"}}", self.desc);
        if let Some(ref e) = self.src {
            write!(f," -> ");
            e.fmt(f)
        } else {
            Ok(())
        }
    }
}

impl From<String> for Error {
    fn from(s: String) -> Self {
        Error {
            desc: s,
            src: None,
            pos: (0,0),
        }}
}

impl <'a> From<&'a str> for Error {
    fn from(s: &'a str) -> Self {
        Error {
            desc: String::from(s),
            src: None,
            pos: (0,0),
        }}
}

