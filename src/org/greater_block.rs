use std::str::Lines;
use org;
use std::str::FromStr;
use std::error::Error;
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Debug,PartialEq)]
pub struct GreaterBlock {
    pub kind: String,
    pub params: Vec<String>,
    pub content: String,
}

impl GreaterBlock {
    pub fn parse(scanner: Rc<RefCell<org::Scanner>>) -> Result<Self,org::Error> {
        let rx_ws = regex!(r"^\s*$");
        let rx_begin = regex!(r#"#\+BEGIN_([A-Z]+)\s+(.*)$"#);
        let rx_end = regex!(r#"^#\+END_([A-Z]+)$"#);
        let mut content:String = String::new();
        let (kind,params) = try!(scanner.borrow_mut()
                .get_line()
                .and_then(|line| rx_begin.captures(line.trim()))
                .and_then(|ref caps|{
                    Some((caps.at(1),
                          caps.at(2).unwrap_or("")
                          .split_whitespace()
                          .map(|s| String::from(s))
                          .collect()))
                })
            .ok_or_else(|| "Invalid greater block #+BEGIN_ line"));
        while let Some(line) = scanner.borrow_mut().get_line() {
            if let Some(ref m) = rx_end.captures(line.trim()) {
                let matchkind = m.at(1).unwrap();
                if kind.map(|s| matchkind != s).unwrap_or(false) {
                    return org::make_err(format!("End block type {} doesn't match begin type {}",
                                                   matchkind, kind.unwrap_or("")).as_str());
                } else if kind.is_none() {
                    return org::make_err("End tag without begin tag for greater block");
                } else {
                    return Ok(GreaterBlock {
                        kind: String::from(kind.unwrap()),
                        params: params,
                        content: content,
                    });
                }
            } else {
                content.extend(line.chars());
            }
        }
        org::make_err("1.21 jiggawatts exceeded. You should never get here.")
    }
}

#[test]
fn testgb(){
    let src = r#"#+BEGIN_SRC c
  if(foo==12){
    printf("bar");
  }
#+END_SRC"#;
    print!("{}",src);
    let mut scanner = Rc::new(RefCell::new(org::Scanner::new(src)));
    let res = GreaterBlock::parse(scanner.clone());
    println!("result: {:?}", res);
    assert!(res.is_ok());
}
