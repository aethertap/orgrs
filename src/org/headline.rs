use org;
use std::str::FromStr;
use std::convert::From;
use std::error::Error;
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Debug,PartialEq)]
pub struct Headline {
    pub level: usize,
    pub todo: Option<TodoTag>,
    pub priority: Option<char>,
    pub title: Option<String>,
    pub tags: Vec<String>,
}

#[derive(Debug,PartialEq)]
pub enum TodoTag {
    Todo,
    Done,
    Defer,
}

impl Headline {
    pub fn parse(scanner: Rc<RefCell<org::Scanner>>) -> Result<Headline,org::Error> {
        if let Some(l) = scanner.borrow_mut().get_line(){
            Headline::from_str(l)
        } else {
            scanner.borrow().make_err("Expected headline, got end of input!")
        }
    } 
}

impl FromStr for TodoTag {
    type Err = org::Error;
    fn from_str(s: &str) -> Result<Self,Self::Err> {
        match s {
            "DONE" => Ok(TodoTag::Done),
            "TODO" => Ok(TodoTag::Todo),
            "DEFER" => Ok(TodoTag::Defer),
            _ => Err(org::Error::from("Error, unknown TODO tag!"))
        }
    }
}

impl FromStr for Headline {
    type Err = org::Error;
    fn from_str(s: &str) -> Result<Self,Self::Err> {
        println!("Parsing headline from {:?}", s);
        let rx = regex!(r#"^(\*+)\s*(TODO|DONE|DEFER)?\s*(\[#(\w)\])?\s*(.*)\s*$"#);
        let tag_rx = regex!(r#":(([A-Za-z0-9@#_%-]+|[^\s]:[^\s])+):\s*$"#);
        if let Some(ref caps) = rx.captures(s) {
            let level = caps.at(1).unwrap().len();
            let todo = caps.at(2).and_then(|cap| TodoTag::from_str(cap).ok());
            let priority = caps.at(4);
            let mut title = caps.at(5).map(|s| s.trim());
            let mut tags:Vec<String> = vec!();
            title.as_mut()
                .map(|t| {
                    if let Some(ref tagcaps) = tag_rx.captures(t) {
                        let tagstr = tagcaps.at(1).unwrap();
                        println!("Tagstr: {}", tagstr);
                        *t = &t[0..tagcaps.pos(0).unwrap().0].trim();
                        tags.extend(tagstr[..].split(':').map(|s| String::from(s)));
                    }
            });
            Ok(Headline{
                level: level,
                todo: todo,
                priority: priority.and_then(|p| p.chars().nth(0)),
                title: title.and_then(|t| if t == "" {None} else {Some(From::from(t))}),
                tags: tags,
            })
        } else {
            Err(org::Error::from("Failed to parse headline"))
        }
    }
}

impl Default for Headline {
    fn default() -> Headline {
        Headline{
            title: None,
            level: 0,
            priority: None,
            todo: None,
            tags: vec!(),
        }
    }
}

#[test]
fn test_all_opts() {
    let hd=r"** TODO [#a] This is a long title with a colon: :and:some:tags:
";
    let hdl=Headline::from_str(hd);
    println!("Headline: {:?}", hdl);
    assert!(hdl.is_ok());
    let h = hdl.unwrap();
    assert!(h.level == 2);
    assert!(h.title.is_some());
    assert!(h.title.unwrap() == "This is a long title with a colon:");
    assert!(h.tags.len() == 3);
    assert!(h.tags[0] == "and");
    assert!(h.tags[1] == "some");
    assert!(h.tags[2] == "tags");
    assert!(h.todo.is_some());
    assert!(h.todo.unwrap() == TodoTag::Todo);
    assert!(h.priority.is_some() && h.priority.unwrap() == 'a');
}

#[test]
fn test_no_pri(){
    let hd=r"****** DONE No priority or tags:";
    let hdl = Headline::from_str(hd);
    assert!(hdl.is_ok());
    let h = hdl.unwrap();
    assert!(h.level==6);
    assert!(h.tags.len()==0);
    assert!(h.title.unwrap() == "No priority or tags:");
    assert!(h.priority.is_none());
    assert!(h.todo.is_some());
    assert!(h.todo.unwrap() == TodoTag::Done);
}

#[test]
fn test_no_todo(){
    let hd=r"* [#b] Priority b and one star. :tags%are:great:  ";
    let hdl = Headline::from_str(hd);
    assert!(hdl.is_ok());
    let h = hdl.unwrap();
    assert!(h.priority.is_some() && h.priority.unwrap() == 'b');
    assert!(h.title.is_some() && h.title.unwrap() == "Priority b and one star.");
    assert!(h.tags.len() == 2);
    assert!(h.tags[0] == "tags%are");
    assert!(h.tags[1] == "great");
    assert!(h.todo.is_none());
    assert!(h.level == 1);
}

#[test]
fn test_no_title(){
    let hd = r"*DEFER [#c] :with_some:tags-and:stuff:";
    let hdl = Headline::from_str(hd);
    assert!(hdl.is_ok());
    let h = hdl.unwrap();
    assert!(h.level==1);
    assert!(h.todo.is_some() && h.todo.unwrap() == TodoTag::Defer);
    assert!(h.priority.is_some() && h.priority.unwrap() == 'c');
    assert!(h.title.is_none());
    assert!(h.tags.len() == 3);
    assert!(h.tags[0] == "with_some");
    assert!(h.tags[1] == "tags-and");
    assert!(h.tags[2] == "stuff");
}
