
#[derive(Debug,PartialEq)]
pub enum Keyword{
    Caption(Vec<Markup>/*optional*/, Vec<Markup>/*value*/),
    Results(Vec<Markup>/*optional*/, Vec<Markup>/*value*/),
    Result(String/*value*/),
    Author(Vec<Markup>/*value*/),
    Date(Vec<Markup>/*value*/),
    Title(Vec<Markup>/*value*/),
    Attr(String/*backend*/,String/*value*/),
    Data(String/*value*/),
    Header(String/*value*/),
    Headers(String/*optional*/,String/*value*/),
    Plot(String/*optional*/,String/*value*/),
    Source(String/*value*/),
    SrcName(String/*value*/),
    TblName(String/*value*/),
    Other(String/*value*/),
}

impl Keyword {
    pub fn parse(scanner: Rc<RefCell<Scanner>>) -> Result<Keyword,org::Error> {
    }
    pub fn parse_all(scanner: Rc<RefCell<Scanner>>) -> Result<Vec<Keyword>,org::Error> {
    }
}
