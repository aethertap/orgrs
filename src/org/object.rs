use org;
use regex::{Regex,RegexSet};
use std::rc::Rc;
use std::cell::RefCell;
use std::collections::HashMap;

pub struct ObjectIter<'a> {
    min_level: usize,
    scanner: Rc<RefCell<org::Scanner<'a>>>,
}

type Keywords = HashMap<String,String>;

#[derive(Debug,PartialEq)]
pub enum Object {
    Paragraph(Vec<org::Markup>),
    Text(String),
    Link(String,String),
    Formula(String),
    FootnoteRef(String),
    FootnoteDef(String,Vec<Object>), // can contain multiple paragraphs
    GreaterBlock(org::GreaterBlock),
    Section(org::Section),
    Drawer(org::Drawer),
    Table(org::Table),
}

impl <'a> ObjectIter<'a>{
    pub fn new(min_level: usize, scanner:Rc<RefCell<org::Scanner<'a>>>) -> ObjectIter<'a> {
        ObjectIter {
            min_level: min_level,
            scanner: scanner.clone(),
        }
    }

    // special function to parse properties, since they can only
    // appear in exactly one location.
    pub fn parse_properties(&mut self) -> Result<Option<HashMap<String,String>>,org::Error> {
        let mut scan = self.scanner.borrow_mut();
        let prop_rx = regex!(r"^:PROPERTIES:\s*$");
        if scan.peek_regex_line(&prop_rx).and_then(|c| c.at(0)).is_some() {
            let _ = scan.get_line(); // eat the header line
            let kv = regex!(r"^:([a-zA-Z0-9_-]+\+?):\s*(.+[^\s])\s*$");
            let end = regex!(r"^:END:\s*$");
            let mut pairs:HashMap<String,String> = HashMap::new();
            while let Some(caps) = scan.get_regex_line(&kv) {
                if let Some((key,val)) = caps.at(1)
                    .and_then(|c1| caps.at(2).map(|c2| (c1,c2))) {
                        pairs.insert(key.to_string(), val.to_string());
                    }
            }
            if scan.get_regex_line(&end).is_none() {
                return scan.make_err("Properties drawer missing its :END: tag");
            }
            Ok(Some(pairs))
        } else {
            Ok(None)
        } 
    }


    // read lines until an empty line or a non-paragraph element is
    // reached.
    fn parse_paragraph(&mut self) -> Option<Result<Object,org::Error>> {
        let enders:RegexSet = RegexSet::new(&[
            r"^\*",                  // headline
            r"^#\+BEGIN_",           // greater block
            r"^#\+BEGIN:",           // dynamic block
            r"^#\+[:word:]+:",       // keyword
            r"^\[fn:[:word:]+\]",    // footnote definition
            r"^:[:word:]+:",         // drawer start
            r"^\s*[:digit:]+\.\s",   // ordered list start
            r"^\s+\*\s+",            // unordered list (must be indented)
            r"^\s*$",                // empty line
            r"^\s*\|",               // Table
            ]).unwrap();
        let mut para = String::new();
        while !self.scanner.borrow().peek_regexset_line(&enders).matched_any() {
            self.scanner.borrow_mut().get_line().map(|l| para.extend(l.chars()));
        }
        if para.len() > 0{
            Some(Ok(Object::Paragraph(org::Markup::parse_all(org::Scanner::makerc(para.as_str())))))
        } else {
            None
        }
    }
}

impl <'a> Iterator for ObjectIter<'a> {
    type Item = Result<Object,org::Error>;
    fn next(&mut self) -> Option<Result<Object,org::Error>> {
        let starters:RegexSet = RegexSet::new(&[
            r"^\*",                  // headline
            r"^#\+BEGIN_",           // greater block
            r"^#\+BEGIN:",           // dynamic block
            r"^#\+[:word:]+:",       // keyword
            r"^\[fn:[:word:]+\]",    // footnote definition
            r"^:[:word:]+:",         // drawer start
            r"^\s*[:digit:]+\.\s",   // ordered list start
            r"^\s+\*\s+",            // unordered list (must be indented)
            r"^\s*\|",               // Table
            ]).unwrap();
        let stars = regex!(r"^\*+");  // for counting stars at headline open
        self.scanner.borrow_mut().eat_blank_lines();
        let maybe_n =  self.scanner.borrow_mut()
            .peek_regexset_line(&starters)
            .into_iter()
            .nth(0);
        if let Some(n) = maybe_n {
            match n {
                0 => {
                    let stars = self.scanner.borrow_mut()
                        .peek_regex(&stars)
                        .and_then(|ref cap| cap.at(0))
                        .unwrap_or("");
                    if stars.len() > self.min_level {
                        let sec = org::Section::parse(self.scanner.clone());
                        Some(sec.map(|s| Object::Section(s)))
                    } else {
                        None
                    }
                },
                1 => Some(org::GreaterBlock::parse(self.scanner.clone())
                          .map(|gb| Object::GreaterBlock(gb))),
                //2 => Object::DynamicBlock(org::DynamicBlock::parse(&mut scanner)),
                //3 => Object::Keyword(org::Keyword::parse(&mut scanner)),
                //4 => Object::FootnoteDef(org::FootnoteDef::parse(&mut scanner)),
                5 => Some(org::Drawer::parse(self.scanner.clone())
                          .map(|drawer| Object::Drawer(drawer))),
                //6 => Object::OrderedList(org::OrderedList::parse(&mut scanner)),
                //7 => Object::UnorderedList(org::UnorderedList::parse(&mut scanner)),
                8 => Some(org::Table::parse(self.scanner.clone()).map(|t| Object::Table(t))),
                _ => Some(self.scanner.borrow().make_err("Unrecognized input")),
            }
        } else { // If there's nothing else here, try a paragraph.
            self.parse_paragraph()
        }
    }
}


#[test]
fn GreaterBlock(){
    let src=r"#+BEGIN_SRC c d e
   void foo();
#+END_SRC";
    let mut scanner = org::Scanner::makerc(src);
    let mut iter = ObjectIter::new(0,scanner);
    let item = iter.next();
    assert!(item.is_some());
    let itm = item.unwrap().ok();
    assert_eq!(itm, Some(Object::GreaterBlock(org::GreaterBlock{
        kind: String::from("SRC"),
        params: vec!["c".to_string(),"d".to_string(),"e".to_string()],
        content: "   void foo();\n".to_string(),
        })));
    assert!(iter.next().is_none());
}

#[test]
fn para(){
    let src=r"this is a 
paragraph with some
text in it.
 
not part of the paragraph.
";
    let mut scanner = org::Scanner::makerc(src);
    let mut iter = ObjectIter::new(0,scanner);
    let objs:Result<Vec<Object>,org::Error> = iter.collect();
    assert!(objs.is_ok());
    assert_eq!(objs.unwrap(), vec![
        Object::Paragraph(vec![org::Markup::Text("this is a \nparagraph with some\ntext in it.\n".to_string())]),
        Object::Paragraph(vec![org::Markup::Text("not part of the paragraph.\n".to_string())])]);
}
