use regex::{Regex,Captures,RegexSet,SetMatches};
use org;
use std::rc::Rc;
use std::cell::RefCell;

pub struct Scanner<'a> {
    data: &'a str,
    pub pos: usize,
}


impl<'a> Scanner<'a> {
    pub fn new(data: &'a str) -> Self {
        Scanner{
            data:data,
            pos:0,
        }
    }

    pub fn makerc(data: &'a str) -> Rc<RefCell<Self>>{
        Rc::new(RefCell::new(Self::new(data)))
    }

    pub fn eat_blank_lines(&mut self) {
        let blank_rx = regex!(r"^(\s*)$");
        while let Some(c) = self.peek_line().and_then(|l| blank_rx.captures(l)) {
            c.pos(0).map(|(_,end)| self.pos = self.pos+end);
        }
    }
    
    pub fn get_line(&mut self) -> Option<&'a str> {
        let line = self.peek_line();
        line.map(|l| self.pos += l.len());
        line
    }

    pub fn get_regex(&mut self, rgx: &Regex) -> Option<Captures<'a> > {
        let caps = self.peek_regex(rgx);
        caps.as_ref().map(|c| c.pos(0).map(|(_,ref end)| self.pos = self.pos+end));
        caps
    }

    // Doesn't consume input unless the line matches.
    pub fn get_regex_line(&mut self, rgx: &Regex) -> Option<Captures<'a>> {
        self.peek_line()
            .and_then(|l| rgx.captures(l.trim_right())
                      .map(|caps| (l.len(),caps)))
            .map(|(len,caps)| {
                self.pos += len;
                caps
            })
    }
   
    pub fn get_char(&mut self) -> Option<char> {
        let result = self.peek_char();
        if result.is_some() {
            self.pos = self.pos+1;
        }
        result
    }

    pub fn peek_line(&self) -> Option<&'a str>{
        if self.pos >= self.data.len() {
            None
        } else {
            let start = self.pos;
            let end = self.data[start..].find('\n').map(|n| n+1);
            if let Some(e) = end {
                Some(&self.data[start..start+e])
            } else {
                Some(&self.data[start..])
            }
        }
    }

    pub fn peek_regex(&self, rgx: &Regex) -> Option<Captures<'a>>{
        let mut data = "";
        if self.pos < self.data.len() {
            data = &self.data[self.pos..];
        }
        rgx.captures(data)
    }

    pub fn peek_regex_line(&self, rgx: &Regex) -> Option<Captures<'a>>{
        self.peek_line()
            .and_then(|l| rgx.captures(l.trim_right()))
    }
    
    pub fn peek_char(&self) -> Option<char> {
        self.data.chars().nth(self.pos)
    }
 
    pub fn peek_regexset(&self, rgxs: &RegexSet) -> SetMatches {
        rgxs.matches(&self.data[self.pos..])
    }

    pub fn peek_regexset_line(&self, rgxs: &RegexSet) -> SetMatches {
        let l = self.peek_line().unwrap_or("");
        rgxs.matches(l)
    }
    
    pub fn unget(&mut self, count: usize) -> Result<(),org::Error> {
        if count > self.pos {
            return Err(org::Error::from(format!("Attempted to unget too many chars (attempted {}, but only {} were available",count,self.pos)));
        }
        self.pos -= count;
        Ok(())
    }

    pub fn position(&self) -> (usize,usize) {
        let mut line = 1;
        let mut col = 1;
        for ch in self.data[0..self.pos].chars() {
            match ch {
                '\n' => {
                    line += 1;
                    col = 1;
                },
                _ => col += 1,
            };
        }
        (line,col)
    }
    
    pub fn make_err<T>(&self,msg: &str) -> Result<T,org::Error>{
        Err(org::Error{desc: msg.to_string(),
                       src: None,
                       pos: self.position(),
        })
    }
}

#[test]
fn test_getline(){
    let mut scanner = Scanner::new("this \n is a \n test\n and a line at the end with no newline");
    let mut line = scanner.get_line();
    println!("Line: {:?}", line);
    assert!(line.is_some() && line.unwrap() == "this \n");
    line = scanner.get_line();
    println!("Line: {:?}", line);
    assert!(line.is_some() && line.unwrap() == " is a \n");
    line = scanner.get_line();
    println!("Line: {:?}", line);
    assert!(line.is_some() && line.unwrap() == " test\n");
    line = scanner.get_line();
    println!("Line: {:?}", line);
    assert!(line.is_some() && line.unwrap() == " and a line at the end with no newline");
    line = scanner.get_line();
    println!("Line: {:?}", line);
    assert!(line.is_none());
}

#[test]
fn test_regex(){
    let r = regex!(r"a*b");
    let emp = regex!(".*");
    let mut scanner = Scanner::new("aaaaaaaaabaabb");
    let mut cap = scanner.get_regex(&r);
    assert!(cap.as_ref().is_some() && cap.as_ref().unwrap().at(0).is_some()
            && cap.as_ref().unwrap().at(0).unwrap() == "aaaaaaaaab");
    cap = scanner.get_regex(&r);
    assert!(cap.as_ref().is_some() && cap.as_ref().unwrap().at(0).is_some()
            && cap.as_ref().unwrap().at(0).unwrap() == "aab");
    cap = scanner.get_regex(&r);
    assert!(cap.as_ref().is_some() && cap.as_ref().unwrap().at(0).is_some()
            && cap.as_ref().unwrap().at(0).unwrap() == "b");
    cap = scanner.get_regex(&r);
    assert!(cap.is_none());
    cap = scanner.get_regex(&emp);
    assert!(cap.is_some());
}

#[test]
fn test_mixed(){
    let s = String::from("this\n is \n      a       \nregegegegegex\nlast");
    let mut scanner = Scanner::new(&s[..]);
    let spacea = regex!(" *a *");
    let rgx = regex!("r[eg]*x");
    let ln = scanner.get_line().map(|l| l=="this\n");
    assert!(ln.is_some() && ln.unwrap());
    let is = scanner.get_line().map(|l| l==" is \n");
    assert!(is.is_some() && is.unwrap());
    let asp = scanner.get_regex(&spacea)
        .and_then(|ref c| c.at(0).map(|s| s == "      a       "));
    assert!(asp.is_some() && asp.unwrap());
    let mut nl = scanner.get_line().map(|l| l=="\n");
    assert!(nl.is_some() && nl.unwrap());
    let rg = scanner.get_regex(&rgx)
        .and_then(|ref c| c.at(0).map(|s| s == "regegegegegex"));
    assert!(rg.is_some() && rg.unwrap());
    nl = scanner.get_line().map(|l| l=="\n");
    assert!(nl.is_some() && nl.unwrap());
    let end = scanner.get_line().map(|l| l=="last");
    assert!(end.is_some() && end.unwrap());
    let empty = scanner.get_line();
    assert!(empty.is_none());
    let empty2 = scanner.get_regex(&spacea);
    assert!(empty2.is_none());
    let r3 = regex!(".*");
    let empty3 = scanner.get_regex(&r3)
        .and_then(|ref c| c.at(0).map(|s| s == ""));
    assert!(empty3.is_some() && empty3.unwrap());
}

#[test]
fn test_char(){
    let mut scanner = Scanner::new("abc\n");
    let mut ch = scanner.get_char();
    assert!(ch.is_some() && ch.unwrap()=='a');
    ch = scanner.get_char();
    assert!(ch.is_some() && ch.unwrap()=='b');
    ch = scanner.get_char();
    assert!(ch.is_some() && ch.unwrap()=='c');
    let ln = scanner.get_line().map(|l| l=="\n");
    assert!(ln.is_some() && ln.unwrap());
    ch = scanner.get_char();
    assert!(ch.is_none());
}
