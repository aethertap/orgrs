
use org;
use org::Scanner;
use std::str::FromStr;
use std::rc::Rc;
use std::cell::RefCell;
// markup is for things that appear inline with other text, like
// links, bold, and latex formulas

#[derive(Debug,PartialEq)]
pub enum Markup{
    Text(String),
    Link(Option<String>/*url*/,Option<String>/*display*/),
    Footnote(String),
    Starred(Vec<Markup>),
    Slanted(Vec<Markup>),
    Underscored(Vec<Markup>),
    Formula(String),
}

// Some of these can have subelements within themselves. For example,
// the stars, slashes, and underscores can all have any of the other
// markups within them. I'd say that the link display text could also
// have anything that's not a link or a footnote.

impl Markup {
    // The scanner here should *only* have the data for a single
    // "paragraph" of marked up text. This function will not respect
    // empty lines, headlines, or anything else. Parse out the markup
    // text first, then make a scanner for it to use here.
    pub fn parse_all(scan: Rc<RefCell<Scanner>>) -> Vec<Markup> {
        let mut scanner = scan.borrow_mut();
        let link_rx = regex!(r#"^\[\[(([^\]]|\]\[)+)\]\]"#);
        let fn_rx = regex!(r#"^\[fn:([^\]]+)\]"#);
        let star_rx = regex!(r#"^\*([^\s]([^*]|\s\*)+[^\s])\*"#);
        let slant_rx = regex!(r#"/([^\s]([^/]|[\s]/)+[^\s])/"#);
        let under_rx = regex!(r#"_([^\s]([^_]|[\s]_)+[^\s])_"#);
        let formula_rx = regex!(r#"^\$([^\s][^$]+[^\s])\$[\s]"#);
        let mut text = String::new();
        let mut all_markup:Vec<Markup> = vec![];
        while let Some(ch) = scanner.peek_char() {
            let markup = match ch {
                '[' => {
                    if let Some(m) = scanner.get_regex(&link_rx) {
                        m.at(1).map(|l| Markup::parse_link(l))
                    } else if let Some(m) = scanner.get_regex(&fn_rx) {
                        m.at(1).map(|f| Markup::Footnote(f.to_string()))
                    } else {
                        None
                    }
                },
                '*' => scanner.get_regex(&star_rx)
                    .and_then(|m| m.at(1)
                              .map(|txt| Markup::Starred(Markup::parse_all(Scanner::makerc(txt))))), 
                '_' => scanner.get_regex(&under_rx)
                    .and_then(|m| m.at(1)
                              .map(|txt| Markup::Underscored(Markup::parse_all(Scanner::makerc(txt))))), 
                '/' => scanner.get_regex(&slant_rx)
                    .and_then(|m| m.at(1)
                              .map(|txt| Markup::Slanted(Markup::parse_all(Scanner::makerc(txt))))),
                '$' => scanner.get_regex(&formula_rx)
                    .and_then(|m| m.at(1).map(|txt| Markup::Formula(txt.to_string()))),
                _ => None,
            };
            if let Some(mark) = markup {
                if text.len() > 0 {
                    all_markup.push(Markup::Text(text));
                    text = String::new();
                }
                all_markup.push(mark);
            } else {
                scanner.get_char().map(|c| text.push(c));
            }
        }
        if text.len() > 0 {
            all_markup.push(Markup::Text(text));
        }
        all_markup
    }

    pub fn parse_link(txt: &str) -> Markup {
        let mut parts = txt.split("][");
        let url = parts.next().map(|s| s.to_string());
        let rest = Some(parts.collect::<Vec<&str>>().join("]["))
            .and_then(|s| if s.len() < 1{None} else {Some(s.to_string())});
        Markup::Link(url,rest)
    }
}

#[test]
fn text(){
    let mut scanner = Scanner::makerc("This is a test of * markup [ with $ some stuff thrown in \n");
    let cmp = vec![Markup::Text("This is a test of * markup [ with $ some stuff thrown in \n".to_string())];
    let result = Markup::parse_all(scanner);
    println!("Parsed: {:?}", result);
    println!("craft:  {:?}", cmp);
    assert_eq!(cmp, result);
}

#[test]
fn text_link(){
    let mut scanner = Scanner::makerc("This is a [[http://foo.com][link]]");
    let cmp = vec![Markup::Text("This is a ".to_string()),
                   Markup::Link(Some("http://foo.com".to_string()), Some("link".to_string()))];
    let result = Markup::parse_all(scanner);
    println!("result: {:?}", result);
    println!("base:   {:?}", cmp);
    assert_eq!(result,cmp);
}

#[test]
fn all(){
    let mut scanner = Scanner::makerc("*bold*/italic/_under_[fn:barb][[link]][[link][desc]]and some text");
    let cmp = vec![
        Markup::Starred(vec![Markup::Text("bold".to_string())]),
        Markup::Slanted(vec![Markup::Text("italic".to_string())]),
        Markup::Underscored(vec![Markup::Text("under".to_string())]),
        Markup::Footnote("barb".to_string()),
        Markup::Link(Some("link".to_string()),
                     None),
        Markup::Link(Some("link".to_string()),
                     Some("desc".to_string())),
        Markup::Text("and some text".to_string()),
        ];
    let result = Markup::parse_all(scanner);
    println!("result: {:?}", result);
    println!("base:   {:?}", cmp);
    assert_eq!(result,cmp);
}

#[test]
fn nested_all(){
    let mut scanner = Scanner::makerc("*a bold [[link][descr]] /slanted _underscored_/ test*");
    let cmp = vec![
        Markup::Starred(vec![
            Markup::Text("a bold ".to_string()),
            Markup::Link(Some("link".to_string()),
                         Some("descr".to_string())),
            Markup::Text(" ".to_string()),
            Markup::Slanted(vec![
                Markup::Text("slanted ".to_string()),
                Markup::Underscored(vec![
                    Markup::Text("underscored".to_string())
                        ]),
                ]),
            Markup::Text(" test".to_string()),
        ])];
    let result = Markup::parse_all(scanner);
    println!("result: {:?}", result);
    println!("base:   {:?}", cmp);
    assert_eq!(result,cmp);
}
