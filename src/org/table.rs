use org::{self,Scanner,Object,Markup,Error,make_err};
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Debug,PartialEq)]
pub struct Table {
    pub attributes: HashMap<String,String>,
    pub rows: Vec<TableRow>,
    pub formulas: Vec<(Option<String>/*row*/,String/*col*/,String/*formula*/)>,
}

#[derive(Debug,PartialEq)]
pub enum TableRow {
    Data(Vec<String>),
    Rule,
}

impl Table {
    pub fn parse(scan: Rc<RefCell<Scanner>>) -> Result<Table,Error>{
        let mut text = String::new();
        let mut scanner = scan.borrow_mut();
        let tbl_rx = regex!(r"^\s*\|(.*)$"); // begin with '|'
        let tblfm_rx = regex!(r"^#\+TBLFM:\s*(.*[^\s])\s*$");
        while let Some(line) = scanner.get_regex_line(&tbl_rx) {
            let mut cleaned = line.at(1).unwrap().trim();
            if cleaned.len() > 1 && cleaned.char_at(cleaned.len()-1) == '|'{
                cleaned = &cleaned[..cleaned.len()-1];
            }
            println!("cleaned: {:?}", cleaned);
            text.extend(cleaned.chars());
            text.push('\n');
        }
        let formulas = if let Some(_) = scanner.peek_regex_line(&tblfm_rx) {
            scanner.get_regex_line(&tblfm_rx)
                .ok_or_else(|| Error::from("Internal error, regex_line failed."))
                .and_then(|caps| Table::parse_formulas(caps.at(1).unwrap()))
        } else {
            Ok(Vec::new())
        };
        formulas.map(|form| Table {
            attributes:HashMap::new(),
            rows: Table::parse_rows(&text[..]),
            formulas: form,
        })
    }

    fn parse_formulas(fstr:&str) -> Result<Vec<(Option<String>,String,String)>,Error> {
        let formula_rx = regex!(r"^(@[0-9+<>-I]+)?(\$[0-9+<>-]+)\s*=\s*(.*[^\s])\s*$");
        println!("fstr: {:?}",fstr);
        fstr.split("::")
            .map(|formula| {
                println!("formula: {:?}",formula);
                formula_rx.captures(formula)
                    .map(|caps| {
                        println!("caps {:?},{:?},{:?}",caps.at(1),caps.at(2),caps.at(3));
                        (caps.at(1).map(|s| String::from(&s[1..])),
                                 String::from(&caps.at(2).unwrap()[1..]),
                         String::from(caps.at(3).unwrap()))
                    })
                    .ok_or_else(|| Error::from("Invalid formula"))
            }).collect()
    }

    fn parse_rows(rowstr: &str) -> Vec<TableRow> {
        let rule_rx = regex!(r"^-+[+|\s-]*$");
        rowstr.lines()
            .map(|line| {
                println!("row: {:?}", line);
                if rule_rx.is_match(line) {
                    TableRow::Rule
                } else {
                    TableRow::Data(
                        line.split('|')
                            .map(|cell| String::from(cell.trim()))
                            .collect())
                }
            })
            .collect()
    }
}

#[test]
fn table1(){
    let mut scanner = Scanner::makerc(r"|asdf |asdf  |  asf
|-
|---+---+---
|3|4|5|
#+TBLFM: $2=1::@2$3=5::@<$<<<=888::@III$>>=77");
    let tbl = Table::parse(scanner);
    let rf = Table{
        attributes:HashMap::new(),
        rows: vec![
            TableRow::Data(vec![String::from("asdf"),String::from("asdf"),String::from("asf")]),
            TableRow::Rule,
            TableRow::Rule,
            TableRow::Data(vec![String::from("3"),String::from("4"),String::from("5")])],
        formulas: vec![(None,String::from("2"),String::from("1")),
                       (Some(String::from("2")),String::from("3"),String::from("5")),
                       (Some(String::from("<")),String::from("<<<"),String::from("888")),
                       (Some(String::from("III")),String::from(">>"),String::from("77"))]
    };
    println!("parsed {:?}",tbl);
    println!("ref    {:?}", rf);
    assert!(tbl.is_ok() && tbl.unwrap() == rf);
}

#[test]
fn empty_cells(){
    let scanner = Scanner::makerc(r"|||");
    let result = Table::parse(scanner);
    let rf = Table{
        attributes:HashMap::new(),
        rows: vec![
            TableRow::Data(vec![String::new(),String::new()]),
            ],
        formulas: vec![],
    };
    println!("compare:");
    println!("result: {:?}",result.as_ref().unwrap());
    println!("ref:    {:?}",rf);
    assert!(result.is_ok() && result.unwrap() == rf);
}
