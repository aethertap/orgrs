use regex::{Regex,RegexSet};
use org::{self,GreaterBlock,Headline,Object,ObjectIter,Scanner};
use std::str::{Lines, FromStr};
use std::collections::HashMap;
use std::rc::Rc;
use std::cell::RefCell;

#[derive(Debug,PartialEq)]
pub struct Section {
    pub headline: Headline,
    pub content: Vec<Object>,
    pub properties: Option<HashMap<String,String>>,
}

// Org is a line-oriented format with a few special cases for things
// like paragraphs and tables. I'll parse it using basic regular
// expressions.


// Section contains everything except a section with a lower
// indentation. Loop parsing objects until a lower section heading is
// encountered.
impl Section {
    pub fn parse<'a>(scanner: Rc<RefCell<Scanner<'a>>>) -> Result<Section,org::Error> {
        let headline = Headline::parse(scanner.clone())
            .unwrap_or_else(|_e| Headline::default());
        let mut inner = ObjectIter::new(headline.level, scanner.clone());
        let props = inner.parse_properties();
        props.and_then(|p| inner.collect::<Result<Vec<Object>,org::Error>>()
                  .map(|content|
                       Section{
                           headline: headline,
                           content: content,
                           properties: p,
                       }))
    }
}

#[test]
fn basic(){
    let src = r#"*** TODO [#a] This is the title          :and:here:are:tags:    
Now for a paragraph full of text and all that great stuff.
** This is the second section
#+BEGIN_SRC c
 void foo();
#+END_SRC
"#;
    let mut scanner = Rc::new(RefCell::new(Scanner::new(src)));
    let mut section = Section::parse(scanner.clone());
    let refsection = org::Section{
        headline: org::Headline {
            title: Some(String::from("This is the title")),
            todo: Some(org::TodoTag::Todo),
            level: 3,
            priority: Some('a'),
            tags: vec![String::from("and"),String::from("here"),String::from("are"),String::from("tags")]
        },
        content: vec![
            Object::Paragraph(vec![org::Markup::Text(String::from("Now for a paragraph full of text and all that great stuff.\n"))]),
        ],
        properties: None,
    };
    print!("Section: {:?}", section);
    assert!(section.is_ok() && section.unwrap()==refsection);
    section = Section::parse(scanner.clone());
    let ref2 = org::Section{
        headline: org::Headline {
            title: Some("This is the second section".to_string()),
            level: 2,
            ..Headline::default()
        },
        content: vec![org::Object::GreaterBlock(org::GreaterBlock {
            kind: "SRC".to_string(),
            params: vec!["c".to_string()],
            content: " void foo();\n".to_string(),
        })],
        properties: None,
    };
    assert!(section.is_ok() && section.unwrap() == ref2)
}

#[test]
fn nested(){
    let src=r#"* awesome title
** level 2
*** level 3
** level 2 again
*** level 3 again
*** level three last time
* next section
"#;
    let mut scanner = Rc::new(RefCell::new(Scanner::new(src)));
    let mut section = Section::parse(scanner.clone());
    let ref1 = Section{
        headline: Headline{
            title: Some("awesome title".to_string()),
            level: 1,
            todo: None,
            priority: None,
            tags: vec!(),
        },
        content: vec![
            Object::Section(Section{
                headline: Headline{
                    title: Some("level 2".to_string()),
                    level: 2,
                    ..Headline::default()
                },
                content: vec![
                    Object::Section(Section {
                        headline: Headline{
                            title: Some("level 3".to_string()),
                            level: 3,
                            ..Headline::default()
                        },
                        content: vec![],
                        properties: None,
                    })],
                properties: None,
            }),
            Object::Section(Section{
                headline: Headline{
                    title: Some("level 2 again".to_string()),
                    level: 2,
                    ..Headline::default()
                },
                content: vec![
                    Object::Section(Section {
                        headline: Headline{
                            title: Some("level 3 again".to_string()),
                            level: 3,
                            ..Headline::default()
                        },
                        content: vec![],
                        properties: None,
                    }),
                    Object::Section(Section {
                        headline: Headline{
                            title: Some("level three last time".to_string()),
                            level: 3,
                            ..Headline::default()
                        },
                        content: vec![],
                        properties: None,
                    })],
                properties: None,
            })],
        properties: None,
    };
    println!("Parsed: {:?}", section.as_ref().unwrap());
    println!("refer:  {:?}", ref1);
    assert!(section.is_ok() && section.unwrap() == ref1); 
}

#[test]
fn with_properties() {
     let src = r#"*** TODO [#a] This is the title          :and:here:are:tags:    
:PROPERTIES:
:author: Erik Lee
:timestamp: <right the eff now>
:END:

Now for a paragraph full of text and all that great stuff.
**** This is the second section
:PROPERTIES:
:author+: Jon
:END:
#+BEGIN_SRC c
 void foo();
#+END_SRC
"#;
    let mut scanner = Rc::new(RefCell::new(Scanner::new(src)));
    let mut section = Section::parse(scanner.clone());
    let mut props:HashMap<String,String> = HashMap::new();
    props.insert("author".to_string(),"Erik Lee".to_string());
    props.insert("timestamp".to_string(), "<right the eff now>".to_string());
    let mut props2:HashMap<String,String>=HashMap::new();
    props2.insert("author+".to_string(), "Jon".to_string());
    let refsection = org::Section{
        headline: org::Headline {
            title: Some(String::from("This is the title")),
            todo: Some(org::TodoTag::Todo),
            level: 3,
            priority: Some('a'),
            tags: vec![String::from("and"),String::from("here"),String::from("are"),String::from("tags")]
        },
        content: vec![
            Object::Paragraph(vec![org::Markup::Text(String::from("Now for a paragraph full of text and all that great stuff.\n"))]),
            Object::Section(org::Section{
                headline: org::Headline {
                    title: Some(String::from("This is the second section")),
                    level: 4,
                    ..Headline::default()
                },
                content: vec![
                    Object::GreaterBlock(org::GreaterBlock{
                        kind: String::from("SRC"),
                        params: vec![String::from("c")],
                        content: String::from(" void foo();\n"),
                    })],
                properties: Some(props2),
            })],
        properties: Some(props),
    };
    println!("Section: {:?}", section.as_ref().unwrap());
    println!("Ref:     {:?}", refsection);
    assert!(section.is_ok() && section.unwrap()==refsection);
}

#[test]
fn no_headline(){
    let src=r"

This is an org file with no headline.

* h1
** h2
*** h3
** h2.2
* h1.2
";
    let scanner = Scanner::makerc(src);
    let result:Result<Section,org::Error> = Section::parse(scanner);
    let rf = Section{
            headline: Headline::default(),
            content: vec![
                Object::Paragraph(vec![
                    org::Markup::Text(String::from("This is an org file with no headline.\n"))]),
                Object::Section(org::Section{
                    headline: Headline{
                        title:Some("h1".to_string()),
                        level: 1,
                        ..Headline::default()
                    },
                    content: vec![
                        Object::Section(org::Section{
                            headline: Headline{
                                title:Some("h2".to_string()),
                                level: 2,
                                ..Headline::default()
                            },
                            content:vec![Object::Section(org::Section{
                                headline: Headline{
                                    title:Some("h3".to_string()),
                                    level: 3,
                                    ..Headline::default()
                                },
                                content:vec![],
                                properties: None,
                            })],
                            properties: None, 
                        }),
                        Object::Section(org::Section{
                            headline: Headline{
                                title:Some("h2.2".to_string()),
                                level: 2,
                                ..Headline::default()
                            },
                            content: vec![],
                            properties: None,
                        })],
                    properties: None,
                }),
                Object::Section(org::Section{
                    headline: Headline{
                        title:Some("h1.2".to_string()),
                        level: 1,
                        ..Headline::default()
                    },
                    content: vec![],
                    properties:None}),
                ],
            properties: None,
        };
    println!("result: {:?}", result.as_ref().unwrap());
    println!("cmp:    {:?}", rf);
    assert!(result.is_ok() && result.unwrap() == rf);
}
